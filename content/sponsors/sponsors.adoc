+++
title = "Corporate Sponsorship"
date = "2021-04-19"
aliases = [ "/sponsors/" ]
categories = [ "Sponsors" ]
weight = 10
summary = "The corporate sponsors that help to support KiCad's development"
[menu.main]
    parent = "Sponsors"
    name   = "Sponsorship"
+++

KiCad relies on the support of our community to build and expand the KiCad EDA platform.  We would like to thank the following Corporate Sponsors who have generously supported KiCad's development this year.

We encourage corporate users of KiCad to support the community by link:{{% ref path="become-a-sponsor.adoc" %}}[joining our Corporate Sponsorship Program].


---

== Platinum Level Sponsors

---

== Gold Level Sponsors


{{< aboutlink "/img/about/digikey-logo.png" "https://www.digikey.com" >}}

=== Digi-Key Electronics

Digi-Key Electronics offers the world’s largest selection of electronic components in stock and available for immediate shipment. From prototype to production, Digi-Key fuels innovation all over the world.

{{< aboutlink "/img/about/aisler-logo.png" "https://aisler.net/partners/kicad" >}}

=== AISLER

AISLER believes that a PCB Design Tool like KiCad should participate financially when a
customer let’s that design become a reality.  That is why AISLER allows its users to easily
donate to the KiCad project during the ordering process.

{{< aboutlink "/img/about/ksc-logo.png" "https://www.kipro-pcb.com/" >}}

=== KiCad Services Corporation

https://www.kipro-pcb.com/[The KiCad Services Corporation] is a full-service commercial
support corporation, formed with the mission of helping our professional users succeed and
thrive with KiCad.  We provide private issue reporting, rapid fixes and online remote desktop
support.  Additionally, we offer contracted feature development options.

---

== Silver Level Sponsors


{{< aboutlink "/img/about/oshpark-logo.png" "https://oshpark.com/" >}}

=== OSHPark

https://oshpark.com/[OSHPark] produces high quality bare printed circuit boards, focused on
the needs of prototyping, hobby design, and light production.  All boards are manufactured
in the United States including 2 and 4 layer rigid PCBs with our classic purple soldermask
as well as 2 layer Flex PCBs and 2 layer rigid with black substrate and clear mask which we
call "After Dark".  We directly accept KiCad board files when ordering.


{{< aboutlink "/img/about/nextpcb-logo.png" "https://www.nextpcb.com/" >}}

=== NextPCB

https://www.nextpcb.com/[NextPCB] has donated to KiCad and is dedicated to one stop solutions
for https://www.nextpcb.com/[PCB prototyping], small batch and mass production, selling and
sourcing of electronic components, SMT and
https://www.nextpcb.com/pcb-assembly-services[PCB assembly].

---

== Bronze Level Sponsors


---

We are grateful to our link:{{% ref path="historical.adoc" %}}[Historical Sponsors] for their past support of the KiCad EDA project