+++
title = "macOS Downloads"
distro = "macOS"
summary = "Downloads for macOS 10.14 thru 10.15"
iconhtml = "<div><i class='fab fa-apple'></i></div>"
weight = 2
aliases = [
    "/download/osx/"
]
+++

[.initial-text]
KiCad is supported on 10.14 and newer.  See
link:/help/system-requirements/[System Requirements] for more details.

[.initial-text]
== Stable Release

Current Version: *5.1.10*

++++

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="mirrors-macos14-heading">
			<h3 class="panel-title">
				<a role="button" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#mirrors-macos14" aria-expanded="true" aria-controls="mirrors-macos14">
					macOS 10.14 and newer
				</a>
			</h3>
		</div>
		<div id="mirrors-macos14" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="mirrors-macos14-heading">
			<div class="panel-body">
				<div class="list-group download-list-group">
					<h4>Worldwide</h4>
					<a class="list-group-item dl-link osdn" href="https://osdn.net/projects/kicad/storage/kicad-unified-5.1.10-1-10_14.dmg">
						<img src="//osdn.net/sflogo.php?group_id=12159&type=1" width="96" height="31"  border="0" alt="OSDN">"OSDN
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>Europe</h4>
					<a class="list-group-item dl-link" href="https://kicad-downloads.s3.cern.ch/osx/stable/kicad-unified-5.1.10-1-10_14.dmg">
						<img src="/img/about/cern-logo.png" /> CERN - Switzerland
					</a>
					<a class="list-group-item dl-link" href="https://www2.futureware.at/~nickoe/kicad-downloads-mirror/osx/stable/kicad-unified-5.1.10-1-10_14.dmg">
						Futureware - Austria
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>China</h4>

					<a class="list-group-item dl-link" href="https://mirrors.cqu.edu.cn/kicad/osx/stable/kicad-unified-5.1.10-1-10_14.dmg">
						<img src="/img/download/chongqing.jpeg" /> Chongqing University
					</a>
					<a class="list-group-item dl-link" href="https://mirrors.dgut.edu.cn/kicad/osx/stable/kicad-unified-5.1.10-1-10_14.dmg">
						<img src="/img/download/dgut.png" />Dongguan University of Technology
					</a>
					<a class="list-group-item dl-link" href="https://mirror.tuna.tsinghua.edu.cn/kicad/osx/stable/kicad-unified-5.1.10-1-10_14.dmg">
						<img src="/img/download/tuna.png" />Tsinghua University
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
++++

[.donate-hidden]
== {nbsp}
++++
	{{< getpartial "download_thanks.html" >}}
++++

== Previous Releases

Previous releases should be available for download on:

https://kicad-downloads.s3.cern.ch/index.html?prefix=osx/stable/


== Nightly Development Builds

The _nightly development_ builds are snapshots of the development (master branch) codebase at a specific time.
This codebase is under active development, and while we try our best, may contain more bugs than usual.
New features added to KiCad can be tested in these builds.

WARNING: These builds may be unstable, and projects edited with these may not be usable with the current stable release. **Use at your own risk**

If you need to test features in active development, the _kicad-nightly_ version can be faster to download, and _kicad-extras_ can be used to turn a _kicad-nightly_ install into a full install.

- *kicad-nightly* does not contain the 3D models.  It contains the KiCad suite, documentation, schematic symbols, footprints, translations, templates, and demos.

- *kicad-extras* contains the 3D models.

*kicad-unified* is the default package that most people should install.  *kicad-unified* contains everything: the KiCad suite, documentation, schematic symbols, footprints, translations, templates, demos, and 3D models.

https://kicad-downloads.s3.cern.ch/index.html?prefix=osx/nightly/
