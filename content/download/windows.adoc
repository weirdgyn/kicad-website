+++
title = "Windows Downloads"
distro = "Windows"
summary = "Downloads for KiCad on Windows 8.1 and 10"
iconhtml = "<div><i class='fab fa-windows'></i></div>"
weight = 2
+++

[.initial-text]
KiCad supports  Windows 8.1 and 10.  See
link:/help/system-requirements/[System Requirements] for more details.

[.initial-text]
== Stable Release

Current Version: *5.1.10*

++++
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="mirrors-64bit-heading">
			<h3 class="panel-title">
				<a role="button" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#mirrors-64bit" aria-expanded="true" aria-controls="mirrors-64bit">
					64-bit (recommended)
				</a>
			</h3>
		</div>
		<div id="mirrors-64bit" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="mirrors-64bit-heading">
			<div class="panel-body">
				<div class="list-group download-list-group">
					<h4>Worldwide</h4>
					<a class="list-group-item dl-link osdn" href="https://osdn.net/projects/kicad/storage/kicad-5.1.10_1-x86_64.exe">
						<img src="//osdn.net/sflogo.php?group_id=12159&type=1" width="96" height="31"  border="0" alt="OSDN"> OSDN
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>Europe</h4>
					<a class="list-group-item dl-link" href="https://kicad-downloads.s3.cern.ch/windows/stable/kicad-5.1.10_1-x86_64.exe">
						<img src="/img/about/cern-logo.png" /> CERN - Switzerland
					</a>
					<a class="list-group-item dl-link" href="https://www2.futureware.at/~nickoe/kicad-downloads-mirror/windows/stable/kicad-5.1.10_1-x86_64.exe">
						Futureware - Austria
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>China</h4>

					<a class="list-group-item dl-link" href="https://mirrors.cqu.edu.cn/kicad/windows/stable/kicad-5.1.10_1-x86_64.exe">
						<img src="/img/download/chongqing.jpeg" /> Chongqing University
					</a>
					<a class="list-group-item dl-link" href="https://mirrors.dgut.edu.cn/kicad/windows/stable/kicad-5.1.10_1-x86_64.exe">
						<img src="/img/download/dgut.png" />Dongguan University of Technology
					</a>
					<a class="list-group-item dl-link" href="https://mirror.tuna.tsinghua.edu.cn/kicad/windows/stable/kicad-5.1.10_1-x86_64.exe">
						<img src="/img/download/tuna.png" />Tsinghua University
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="mirrors-32bit-heading">
			<h3 class="panel-title">
				<a role="button" class="collapsed accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#mirrors-32bit" aria-expanded="false" aria-controls="mirrors-32bit">
					32-bit
				</a>
			</h3>
		</div>
		<div id="mirrors-32bit" class="panel-collapse collapse" role="tabpanel" aria-labelledby="mirrors-32bit-heading">
			<div class="panel-body">
				<div class="list-group download-list-group">
					<h4>Worldwide</h4>
					<a class="list-group-item dl-link osdn" href="https://osdn.net/projects/kicad/storage/kicad-5.1.10_1-i686.exe">
						<img src="//osdn.net/sflogo.php?group_id=12159&type=1" width="96" height="31"  border="0" alt="OSDN">" /> OSDN
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>Europe</h4>
					<a class="list-group-item dl-link" href="https://kicad-downloads.s3.cern.ch/windows/stable/kicad-5.1.10_1-i686.exe">
						<img src="/img/about/cern-logo.png" /> CERN - Switzerland
					</a>
					<a class="list-group-item dl-link" href="https://www2.futureware.at/~nickoe/kicad-downloads-mirror/windows/stable/kicad-5.1.10_1-i686.exe">
						Futureware - Austria
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>China</h4>

					<a class="list-group-item dl-link" href="https://mirrors.cqu.edu.cn/kicad/windows/stable/kicad-5.1.10_1-i686.exe">
						<img src="/img/download/chongqing.jpeg" /> Chongqing University - China
					</a>
					<a class="list-group-item dl-link" href="https://mirrors.dgut.edu.cn/kicad/windows/stable/kicad-5.1.10_1-i686.exe">
						<img src="/img/download/dgut.png" />Dongguan University of Technology - China
					</a>
					<a class="list-group-item dl-link" href="https://mirror.tuna.tsinghua.edu.cn/kicad/windows/stable/kicad-5.1.10_1-i686.exe">
						<img src="/img/download/tuna.png" />Tsinghua University - China
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
++++

[.donate-hidden]
== {nbsp}
++++
	{{< getpartial "download_thanks.html" >}}
++++



== Download Verification
All installer binaries will have a code signed digital signature attached. Windows will automatically verify the signature is valid, however you may want
to ensure it actually is present and the correct signer. A guide on how to verify the installer is available here: link:/help/windows-download-verification/[Windows Installer Verification Guide]

A valid official KiCad signature has:

[role="table table-striped table-condensed"]
|===
|Signer Name|*Simon Richter*
|Issuer|*DigiCert SHA2 Assured ID Code Signing CA*
|Serial Number|*08d72a8ce400d3b9384a263511bac45d*
|===


The nightly build KiCad signature is:

[role="table table-striped table-condensed"]
|===
|Signer Name|*KiCad Services Corporation*
|Issuer|*Sectigo RSA Code Signing CA*
|Serial Number|*1f70b098b5c21a254a6fb427cdf8893e*
|===

== Previous Releases

Previous releases should be available for download on:

https://kicad-downloads.s3.cern.ch/index.html?prefix=windows/stable/


== Testing Builds

The _testing_ builds are snapshots of the current stable release codebase at a specific time.
These contain the most recent bugfixes that will be included in the next stable release.

https://kicad-downloads.s3.cern.ch/index.html?prefix=windows/testing/5.1/


== Nightly Development Builds

The _nightly development_ builds are snapshots of the development (master branch) codebase at a specific time.
This codebase is under active development, and while we try our best, may contain more bugs than usual.
New features added to KiCad can be tested in these builds.

WARNING: These builds may be unstable, and projects edited with these are not usable with the current stable release. **Use at your own risk**

https://kicad-downloads.s3.cern.ch/index.html?prefix=windows/nightly/
